const { src, dest } = require("gulp");
const jsConcat = require("gulp-concat");
const uglify =require("gulp-uglify");

const concatjs = () => {
    return src("./src/js/*.js")
        //.pipe(uglify())
        .pipe(jsConcat("scripts.min.js"))
        .pipe(dest("./dist/js/"));
};

exports.concatjs = concatjs;