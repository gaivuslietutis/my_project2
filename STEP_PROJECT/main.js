
let tabTriggerBtns = document.querySelectorAll('.services-list li button');
console.log(tabTriggerBtns);
tabTriggerBtns.forEach(function(tabTriggerBtn, index){
    tabTriggerBtn.addEventListener('click', function(){
        var currentTabData = document.querySelector('.tab-content[data-tab-content="' + this.dataset.tabTrigger + '"]');
        document.querySelector('.tab-content.is-open').classList.remove('is-open');
        document.querySelector('.services-list li button.is-active').classList.remove('is-active');
        currentTabData.classList.add('is-open');
        this.classList.add('is-active');
    });
});


(function() {
    let links = document.querySelectorAll('.work-list li a');
    console.log(links);
    let imageContainer = document.querySelector('.work-image-galery');
    console.log(imageContainer);
    let imagesCollection = document.querySelectorAll('.work-image-galery img');
    console.log(imagesCollection);
    function displayImage(imgs, album) {
        imgs.forEach((image) => {
            if(image.dataset.album === album) {
                image.classList.remove('hide');
            } else {
                image.classList.add('hide');
            }
        });
    }
    links.forEach((link) => {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            switch (link.textContent) {
                case "Graphic Design":
                    displayImage(imagesCollection, 'Graphic Design');
                    break;
                case "Web Design":
                    displayImage(imagesCollection, 'Web Design');
                    break;
                case "Landing Pages":
                    displayImage(imagesCollection, 'Landing Pages');
                    break;
                case "Wordpress":
                    displayImage(imagesCollection, 'Wordpress');
                    break;

                    case "All":

                    imagesCollection.forEach((image) => {
                        image.classList.remove('hide');
                    });
                    break;
            }
        });
    });
})();


let addButton  = document.getElementById("addBtn");
let content = document.getElementById("content");

addButton.addEventListener("click", function(e) {
    content.classList.toggle("show");
    e.preventDefault();
    this.style.display = "none";
});


let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}
function currentSlide(n) {
    showSlides(slideIndex = n);
}
function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
}


