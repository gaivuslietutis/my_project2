const { src, dest } = require("gulp");
const cssConcat = require("gulp-concat");
const uglify =require("gulp-uglify");
const cleanCss = require("gulp-clean-css");


const concat = () => {
    return src("./src/scss/*.scss")
        .pipe(cssConcat("styles.min.css"))
 //       .pipe(uglify())
        .pipe(cleanCss({compatibility: "ie8"}))
        .pipe(dest("./dist/css"))

};

exports.concat= concat;