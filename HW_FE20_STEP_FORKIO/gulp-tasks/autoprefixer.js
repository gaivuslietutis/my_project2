const {src, dest} = require('gulp');
const autoprefixer = require('gulp-autoprefixer');

const autoprefix = () => {
    return src(['./src/scss/*.scss', "!./src/scss/main.scss"])
        .pipe(autoprefixer(
            {
            cascade: true
        }))
        .pipe(dest('./dist/css')
)
};

exports.autoprefix =autoprefix;