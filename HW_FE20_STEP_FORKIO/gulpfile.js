const { parallel, series } = require("gulp");


const serveTask = require("./gulp-tasks/serve").serve;
const watchTask = require("./gulp-tasks/watch").watch;
const scriptsTask = require("./gulp-tasks/scripts").scripts;
const stylesTask = require("./gulp-tasks/styles").styles;
const concatStyles = require("./gulp-tasks/concatstyles").concat;
const concatJs =require("./gulp-tasks/concatjs").concatjs;
const imageMin =require("./gulp-tasks/images.js").image;


const autoprefixCss =require("./gulp-tasks/autoprefixer.js").autoprefix;

exports.dev = parallel(serveTask, watchTask, series(stylesTask, scriptsTask));
exports.build = series(stylesTask, scriptsTask, concatStyles, concatJs, imageMin, autoprefixCss);
