//«Экранирование символа», это такой процесс, когда естъ нужда найти какой нибудь конкретный символ. Например, если
// мы хотим найти буквально точку. Для этого используется, например, обратная косая черта.
//Есть и другие специальные символы, которые имеют особое значение в регулярном выражении. [ \ ^ $ . | ? * + ( ).
//



function createNewUser() {
    let firstName = prompt("enter name", "");
    let lastName = prompt("enter last name", "");
    let birthday = prompt("Enter birthday", "dd/mm/yyyy");
    let year = Number(birthday.substr(6, 4));
    let month = Number(birthday.substr(3, 2)) - 1;
    let day = Number(birthday.substr(0, 2));
    let today = new Date();
    let age = today.getFullYear() - year;
    if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
        age--;
    }
    alert(age);

    let newUser = {
        firstName: (firstName),
        lastName: (lastName),
        birthday: (birthday),
        age: (age),
        getAge: function () {
            return (this.age)
        },
        getLogin: function () {
            return (this.firstName.slice(0, 1) + this.lastName).toLowerCase() + this.birthday.substr(6, 4 );
        }
    };

    return(newUser)
}

const user1 = createNewUser();

console.log(user1.getLogin());
console.log(user1.age);




