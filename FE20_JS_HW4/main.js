"use strict";

//Films + Characters attempt.

function getDataServer() {
    let url = "https://swapi.dev/api/films/";
    fetch(url)
        .then((response) => {
            return response.json();
        }).then((data) => {
        renderFilms(data);
        }).catch(function (error) {
        console.log(error);
    });
}

getDataServer();

let domList = function () {
    let ul = document.createElement("ul");
    let li = document.createElement("li");
    let title = document.createElement("h3");
    let episode = document.createElement("p");
    let openingCrawl = document.createElement("p");
    let characters = document.createElement("p");
    let names = document.createElement("p");
    li.append(episode, title, openingCrawl, characters, names);
    return {ul, li};
};

let renderFilms = function ({results}) {
    const {ul, li} = domList();

    results.forEach((item, index) => {
        const {title, episode_id, opening_crawl, characters} = item;

        Promise.all(
            characters.map(url => {
                return fetch(url)
                    .then(response => response.json())
                    .then(data => {

                        for (let key in data) {
                            const {name} = data;
                            console.log(name)
                        }
                    })
            }));

        let listItem = li.cloneNode(true);
        listItem.children[1].textContent = title;
        listItem.children[2].textContent = characters;
        listItem.children[4].textContent = name;
        listItem.children[0].textContent = episode_id;
        listItem.children[3].textContent = opening_crawl;
        console.log(listItem.children);
        ul.append(listItem);


    });
    document.body.append(ul);
};


//Characters list + film.

// const baseURL="http://swapi.dev/api/";
// function getPeople(baseURL) {
//     fetch(baseURL)
//         .then(
//             function (u) {
//            return u.json();
//             }
//         ).then(function (json) {
//         renderPeopleList(json);
//         console.log(json);
//     })
//         .catch(function (error) {
//             console.log(error);
//         });
// }
//
// function renderPeopleList({results}) {
//     const listNames = document.createElement("ul");
//     results.forEach(({ name, films }) => {
//         const nameItem = document.createElement("li");
//         nameItem.textContent = name +":  "+films;
//         listNames.append(nameItem);
//     });
//     root.append(listNames);
// }
//
// const peopleData = getPeople(baseURL + "people/");
// console.log(getPeople(baseURL + "people/"));

