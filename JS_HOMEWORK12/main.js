let i = 0;
let images = [];
let time = 10000;

images[0] = "img/img_1.jpg";
images[1] = "img/img_2.jpg";
images[2] = "img/img_3.JPG";
images[3] = "img/img_4.png";

window.onload = function () {
    changeImg();
};

function changeImg() {
    document.slide.src = images[i];

    if (i < images.length - 1) {
        i++;
    } else {
        i = 0;
    }
    let timerId = setTimeout("changeImg()", time);
    document.getElementById("btn-stop").onclick = function () {
        clearTimeout(timerId);
    };
    document.getElementById("btn-resume").onclick = function(){
        clearInterval(timerId);
        timerId = setInterval(function(){changeImg(images)}, time);
    }
}


