"use strict";

let btn = document.getElementById("btn");

btn.addEventListener("click", async () => {
    try {
        const response = await fetch(`https://api.ipify.org/?format=json`);
        const result = await response.json();
        localStorage.setItem("ip", result.ip);
        console.log(`Fetched IP: ${JSON.stringify(result)}`);
    } catch (error) {
        console.log(`Fetch error: ${error.name}`);
    }

    async function fetchDataFromApi(u) {
        let response = await fetch(`http://ip-api.com/json/193.0.216.188`);
//      let response = await fetch(`http://ip-api.com/json/?${result.ip}`);
        let results = await response.json();
        console.log(results);
        return results
    }

    fetchDataFromApi()
        .then(({country, city, countryCode, timezone, region}) => {
            let location = document.createElement("p");
            location.innerText = `${timezone}, ${country}, ${countryCode}, ${city}, region:${region}.`;
            document.body.append(location);
        }).catch(error => {
        console.log(error)
    });

});