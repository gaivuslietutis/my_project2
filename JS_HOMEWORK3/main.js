//Методы - это функции, хранящиеся в виде свойств объекта. Методы JavaScript это действия, которые можно выполнить с объектами.
//Если свойства объекта — это переменные закрепленные за объектом, то методы — это функции закрепленные за объектом.
// Метод JavaScript это свойство, содержащее определение функции.


function createNewUser() {
    let firstName = prompt("enter name", "");
    let lastName = prompt("enter last name", "");

    let newUser = {
        firstName: (firstName),
        lastName: (lastName),
        getLogin: function () {
            return (this.firstName.slice(0, 1) + " " + this.lastName).toLowerCase();
        }
    };

return(newUser)
}

const user1 = createNewUser();
// const user2 = createNewUser();

console.log(user1.getLogin())
// console.log(user2.getLogin())

// console.log(createNewUser());
//console.log(newUser.getLogin())


