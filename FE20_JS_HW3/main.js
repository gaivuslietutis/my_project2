"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set salary(salary) {
        if (salary > 500) {
            this._salary = salary;
        } else {
            this._salary = "intern";
            console.log(`${this.name} is intern.`)
        }
    }

    get salary() {
        return this._salary
    }
}

let empl1 = new Employee("Alex", 23, 600);
let empl2 = new Employee("Anna", 35, 1500);
let empl3 = new Employee("Ronny", 20, 300);
let empl4 = new Employee("Nigel", 40, 1800);

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set lang(lang) {
        let progLang = ["Javascript", "Phyton", "Java", "SQL"];
        if (progLang.includes(lang)) {
            this._lang = lang;
        } else {
            this._lang = "Tester";
            console.log(`${this.name} is supporter in testing an documentation`);
        }
    }

    get lang() {
        return this._lang;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }

    get salary() {
        return this._salary
    }

}

let prog1 = new Programmer("Andrew", 45, 3000, "Phyton");
let prog2 = new Programmer("Mike", 30, 2000, "Java");
let prog3 = new Programmer("John", 32, 1500, "Javascript");
let prog4 = new Programmer("Mary", 28, 1000, "SQL");
let prog5 = new Programmer("Andrew", 45, 800, "JEST");

console.log(prog1);
console.log(prog2);
console.log(prog3);
console.log(prog4);
console.log(prog5);