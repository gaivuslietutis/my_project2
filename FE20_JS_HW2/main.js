"use strict";

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

try {
    for (let i = 0; i <= books.length; i++) {
        if (!books[i].author) {
            throw new SyntaxError("Incomplete data: An author must be included"); // (*)
        }
    }
} catch (e) {
    console.log(e.message);
    console.log(e.name);
}

try {
    for (let i = 0; i <= books.length; i++) {
        if (!books[i].name) {
            throw new SyntaxError("Incomplete data: A name must be included"); // (*)
        }
    }
} catch (e) {
    console.log(e.message);
    console.log(e.name);
}

try {
    for (let i = 0; i <= books.length; i++) {
        if (!books[i].price) {
            throw new SyntaxError("Incomplete data: A price must be included"); // (*)
        }
    }
} catch (e) {
    console.log(e.message);
    console.log(e.name);
};

function displayBooks(arr) {
    const divEl = document.getElementById("root");
    let ul = document.createElement("ul");
    divEl.appendChild(ul);
    for (let i = 0; i <= arr.length; i++) {
        let arrItem = arr[i];
        if ((arrItem.author && arrItem.name && arrItem.price) !== undefined) {
            ul.innerHTML += "Author: " + arrItem.author + " Name: " +
                arrItem.name + " Price: " + arrItem.price + "<br>";
        }
    }
}
const showBooks = displayBooks(books);
console.log(showBooks);

